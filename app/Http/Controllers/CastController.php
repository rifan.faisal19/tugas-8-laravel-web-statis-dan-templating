<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Cast;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cast = Cast::all();
        return view('page.data-cast', compact('cast'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('page.create-cast');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
    		'nama' => 'required|max:255',
    		'umur' => 'required',
            'bio' => 'required|max:255'
    	],
        [
            'nama.required' => 'Nama Harus Diisi',
            'nama.max' => 'Nama Tidak boleh lebih dari 255 karakter',
            'umur.required'  => 'Umur Wajib diisi',
            'bio.required'  => 'Biodata tidak boleh kosong',
            'bio.max'  => 'Biodata tidak boleh lebih 255 karakter'
        ]
        );

        $cast = new Cast;
 
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
 
        $cast->save();

        return redirect('/cast');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cast = Cast::find($id);
        return view('page.show-cast', compact('cast'));  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cast = Cast::find($id);
        return view('page.edit-cast', compact('cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'nama' => 'required|max:255',
    		'umur' => 'required',
            'bio' => 'required|max:255'
    	],
        [
            'nama.required' => 'Nama Harus Diisi',
            'nama.max' => 'Nama Tidak boleh lebih dari 255 karakter',
            'umur.required'  => 'Umur Wajib diisi',
            'bio.required'  => 'Biodata tidak boleh kosong',
            'bio.max'  => 'Biodata tidak boleh lebih 255 karakter'
        ]
        );

        $cast = Cast::find($id);
        $cast -> nama = $request->nama;
        $cast ->umur = $request->umur;
        $cast ->bio = $request->bio;
        $cast ->update();
        return redirect('/cast');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cast = Cast::find($id);
        $cast -> delete();
        return redirect('/cast');
    }
}
