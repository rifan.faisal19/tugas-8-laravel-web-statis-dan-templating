<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('page.register');
    }

    public function send (Request $request){
        $first_name = $request ['nama_depan'];
        $last_name = $request ['nama_belakang'];
       
        return view('page.welcome', compact("first_name", "last_name" ));

    }

}
