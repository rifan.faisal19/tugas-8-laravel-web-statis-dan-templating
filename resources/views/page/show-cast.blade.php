@extends('page.master')

@section('title')
Halaman Detail Pemain Film
@endsection

@section('content')
<h2> Detail Info Pemain {{$cast->id}} </h2>
<h4> Nama Lengkap : {{$cast->nama}}</h4>
<h4> Umur : {{$cast->umur}}</h4>
<h4>Biodata : {{$cast->bio}}</h4>
@endsection
