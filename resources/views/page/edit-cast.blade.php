@extends('page.master')

@section('title')
Form Edit Pemain Film
@endsection

@section('content')

<form action="/cast/ {{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label >Nama</label>
      <input type="text" class="form-control" name="nama" value="{{$cast->nama}}">
         @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for=>Umur</label>
        <input type="number" class="form-control" name="umur" value="{{$cast->umur}}">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
      <label>Biodata</label>
      <textarea class="form-control" name="bio" rows="3">{{$cast->bio}}</textarea>
      @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Simpan</button>
  </form>

@endsection