@extends('page.master')

@section('title')
Form Tambah Pemain Film
@endsection

@section('content')

<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label >Nama</label>
      <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap">
         @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for=>Umur</label>
        <input type="number" class="form-control" name="umur" placeholder="Umur">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
      <label>Biodata</label>
      <textarea class="form-control" name="bio" rows="3"></textarea>
      @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Simpan</button>
  </form>

@endsection