@extends('page.master')
   
   @section('title')
    Halaman Data Table 
   @endsection

   @push('scripts')

    <script src="{{asset('template/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
    $("#example1").DataTable();
    });
    </script>
       
   @endpush

   @section ('content')

   <a href="/cast/create" class="btn btn-primary"> Tambah Pemain</a>
  
    <div class="card-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>#</th>
          <th>Nama</th>
          <th>Umur</th>
          <th>Biodata</th>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key=>$value)
            <tr>
                <td> {{$key + 1}} </td>
                <td> {{$value->nama}} </td>
                <td> {{$value->umur}} </td>
                <td> {{$value->bio}} </td>
                <td> 
                    <form action="/cast/{{$value->id}}" method="POST">
                        @csrf
                        <a href="/cast/{{$value->id}}" class="btn btn-info">Info</a>
                        <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td> 
                
            </tr>
                
                
            @empty
                
            @endforelse
        </tbody>
        <tfoot>
        <tr>
            <th>#</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Biodata</th>
            <th>Action</th>
        </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.card-body -->

</div>  
   @endsection

